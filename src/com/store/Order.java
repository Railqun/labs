package com.store;

import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

public class Order implements Serializable {
    private UUID id;
    private OrderStatus status;
    private Credentials person;
    private ShopingCart basket;
    private long start;
    private long diff;
    private long finish;


    public void create_order(Credentials per, ShopingCart shop, Orders orr) {
        id = UUID.randomUUID();
        status = OrderStatus.AWAITING;
        person = per;
        basket = shop;
        start = System.currentTimeMillis();

        Random rand = new Random();
        diff = 1000 + rand.nextInt(5000);
        finish = start + diff;

        orr.add(this);
    }

    boolean check_time(long data_check) {
        /*System.out.println("time_1 \t\t" + start);
        System.out.println("diff \t\t" + diff);
        System.out.println("time_finish \t" + finish);*/

        if((start + diff) < data_check) {
            status = OrderStatus.PROCESSSED;
            return true;
        }
        else
            return false;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public long getStart() {
        return start;
    }

    public long getDiff() {
        return diff;
    }

    public Credentials getPerson() {
        return person;
    }

    public long getFinish() {
        return finish;
    }

    public UUID getId() { return id; }
}



enum OrderStatus {
    AWAITING, PROCESSSED
}

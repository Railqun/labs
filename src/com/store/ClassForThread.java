package com.store;

import java.util.*;

public class ClassForThread extends ACheck {
    List<Clothes> list_all;
    HashMap<Long, Order> TimeCreate;
    Orders<Order> orr;

    ManagerOrderFile mOrderFile;
    ManagerOrderJSON mOrderJSON;

    boolean valueGen = false;
    boolean valueProc = false;
    boolean valueStop = false;

    public ClassForThread() {
        list_all = new ArrayList<>();
        TimeCreate = new HashMap<>();
        orr = new Orders<>();

        int i, j;

        //генерация всех объектов
        for(i = 0; i < 50; i++) {
            Clothes q;

            if(i % 2 == 0) q = new Shirt();
            else q = new Cap();

            list_all.add(q);
            //q.read();
            //System.out.println("Доступные товары");
            //System.out.println(q.getId());
        }
    }

    synchronized public void gen_orders() {
        int i, j;

        //генерация пользователей с их товарами
        for(i = 0; i < 5; i++) {
            //System.out.println("Создание пользователя " + i);
            Credentials per = new Credentials();
            ShopingCart<Clothes> basket = new ShopingCart<>();

            //System.out.println("Добавление объектов в корзину");
            for(j = 0; j < 10; j++)
                basket.add(list_all.get(i * 10 + j));

            //System.out.println("Удаление объекта");
            basket.delete(i);
            //показать все объекты в корзине
            //basket.view_elements();

            //System.out.println("Создание заказа");
            Order item = new Order();
            item.create_order(per, basket, orr);
            TimeCreate.put(item.getFinish(), item);
        }

        //показать все заказы
        orr.view_elemensts();

        Orders<Order> neworr = new Orders<>();
        Order el = orr.getByNumberList(3);
        //if(el == null) el = orr.getByNumberList(0);
        UUID elId = el.getId();

        mOrderFile = new ManagerOrderFile(orr);
        mOrderJSON = new ManagerOrderJSON(orr);

        //сериализация всех заказов
        mOrderFile.saveAll(orr);
        System.out.println("Вывод десериализации всех заказов");
        neworr = mOrderFile.readAll();
        neworr.view_elemensts();

        //сериализация заказа с id
        mOrderFile.saveById(orr, elId);
        System.out.println("Десериализация заказа с id " + elId);
        mOrderFile.readById(elId);

        UUID noElid = UUID.randomUUID();
        System.out.println("Десериализация заказа с id " + noElid);
        mOrderFile.readById(noElid);

        //сохранение всех заказов в JSON
        System.out.println("\nJSON");
        System.out.println("Запись всех заказов в файл в формате JSON");
        mOrderJSON.saveAll(orr);
        neworr = mOrderJSON.readAll();
        neworr.view_elemensts();

        //сериализация заказа с id
        mOrderJSON.saveById(orr, elId);
        System.out.println("Поиск заказа из файла JSON с id " + elId);
        mOrderJSON.readById(elId);

        UUID noElid2 = UUID.randomUUID();
        System.out.println("Поиск заказа из файла JSON с id" + noElid2);
        mOrderJSON.readById(noElid2);

        valueGen = true;
    }

    @Override
    synchronized public void check_awaiting() {
        if(valueGen) {
            //обработка заказов
            System.out.println("Обработка заказов");
            orr.check_await();

            //показать все заказы после обработки
            orr.view_elemensts();
        }
    }

    @Override
    synchronized public void check_processed() {
        while(valueGen && valueProc) {
            //удаление элементов, у которых статус "обработан"
            System.out.println("Удаление заказов со статусом \"обработан\"");
            orr.check_process();

            //показать все заказы после обработки
            orr.view_elemensts();

            valueProc = false;
        }
    }

    synchronized public void proc_true() {
        valueProc = true;
    }
}

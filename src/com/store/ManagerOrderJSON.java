package com.store;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.Scanner;
import java.util.UUID;

public class ManagerOrderJSON extends AManageOrder {

    public ManagerOrderJSON(Orders<Order> orr) {
    }
    @Override
    public Order readById(UUID id) {
        Orders<Order> orr = readAll();
        Order el = orr.getElByID(id);
        if(el != null) {
            System.out.println("Заказ из файла");
            System.out.println(el.getId() + " time_finish " + el.getFinish() +
                    " " + el.getStatus());
        }
        else {
            System.out.println("В файле нет заказа с id " + id);
        }
        return el;
    }

    @Override
    public void saveById(Orders<Order> orr, UUID id) {
        Order el = orr.getElByID(id);

        if(el == null) {
            System.out.println("Нет такого заказа id " + id);
        }
        else {
            //Gson gson = new Gson();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String el_json = gson.toJson(el);

            try {
                PrintWriter out = new PrintWriter("lab5_order_id_json.bin", "UTF-8");
                out.println(el_json);
                out.close();
            } catch (IOException e) {e.printStackTrace();}
        }
    }

    //@Override
    public Orders<Order> readAll()  {
        Orders<Order> orr = new Orders<>();

        try{
            BufferedReader in = new BufferedReader(new FileReader("lab5_json.bin"));
            //Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Gson gson = new Gson();
            StringBuilder el_json = new StringBuilder();
            String tmp;

            while ((tmp = in.readLine()) != null) {
                el_json.append(tmp);
            }

            //System.out.println(el_json);

            Type type = new TypeToken<Orders<Order>>(){}.getType();
            orr = gson.fromJson(el_json.toString(), type);
            in.close();
        } catch (IOException e) {e.printStackTrace();}

        return orr;
    }

    //@Override
    public void saveAll(Orders<Order> orr) {
        //Gson gson = new Gson();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String el_json = gson.toJson(orr);
        System.out.println(el_json);

        try {
            PrintWriter out = new PrintWriter("lab5_json.bin", "UTF-8");
            out.println(el_json);
            out.close();
        } catch (IOException e) { e.printStackTrace();}
    }
}


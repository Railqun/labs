package com.store;

import java.util.Scanner;

public class Main_5 {
    public static void main(String[] args) {
        ClassForThread q = new ClassForThread();
        Generator gen = new Generator(q);
        //ThreadAwaiting await = new ThreadAwaiting(q, 5000);
        //ThreadProcessed proc = new ThreadProcessed(q);

        Scanner in = new Scanner(System.in);


        System.out.println("Нажмите 1 - проверка на обработанные \n " +
                "2 - выход");
        int enter = 0;


        while (enter != 2) {
            enter = Integer.parseInt(in.nextLine());

            if(enter == 1) {
                q.proc_true();
            }
        }

        q.valueStop = true;
        /*try {
            await.t.join();
            proc.t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        System.out.println("Дочерние потоки завершены, завершение работы программы");
    }
}

package com.store;

public class ThreadAwaiting implements Runnable {
    Thread t;
    ClassForThread q;
    int time;

    ThreadAwaiting(ClassForThread list, int ttime) {
        q = list;
        t = new Thread(this, "Awaiting");
        time = ttime;
        t.start();
    }

    @Override
    public void run() {
        while(!q.valueStop) {
            q.check_awaiting();
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.store;

import java.util.UUID;

public interface IOrder {
    Order readById(UUID id) ;
    void saveById(Orders<Order> orr, UUID id);
    Orders<Order> readAll();
    void saveAll(Orders<Order> orr);
}

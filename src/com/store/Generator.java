package com.store;

public class Generator implements Runnable {
    ClassForThread q;
    Thread t;

    Generator(ClassForThread list) {
        q = list;
        t = new Thread(this, "Генератор");
        t.start();
    }

    @Override
    public void run() {
        q.gen_orders();
    }
}

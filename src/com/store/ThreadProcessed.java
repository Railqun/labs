package com.store;

public class ThreadProcessed implements Runnable {
    Thread t;
    ClassForThread q;

    ThreadProcessed(ClassForThread list) {
        q = list;
        t = new Thread(this, "Processed");
        t.start();
    }

    @Override
    public void run() {
        while(!q.valueStop) {
            q.check_processed();
        }
    }
}

package com.store;

import java.io.*;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder  {
    public ManagerOrderFile(Orders<Order> orr) {
    }

    @Override
    public Order readById(UUID id) {
        Orders<Order> orr = readAll();
        Order el = orr.getElByID(id);
        if(el != null) {
            System.out.println("Заказ из файла");
            System.out.println(el.getId() + " time_finish " + el.getFinish() +
                    " " + el.getStatus());
        }
        else {
            System.out.println("В файле нет заказа с id " + id);
        }
        return el;
    }

    @Override
    public void saveById(Orders<Order> orr, UUID id) {
        Order el = orr.getElByID(id);
        if(el == null) {
            System.out.println("Нет такого заказа id " + id);
        }
        else {
            try {
                ObjectOutputStream in = new ObjectOutputStream(
                        new FileOutputStream("lab5_order_id.bin"));
                try {
                    in.writeObject(el);
                    in.close();
                } finally {in.close(); }
                System.out.println("Заказ id " + id + " сохранен");
            }
            catch (FileNotFoundException e) {
                e.printStackTrace() ;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //@Override
    public Orders<Order> readAll()  {
        Orders<Order> orr = new Orders<>();

        try{
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("lab5.bin"));
            try{
                orr = (Orders<Order>) in.readObject();
        } finally {
                in.close();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace(); }

            return orr;
    }

    //@Override
    public void saveAll(Orders<Order> orr) {
            try {
                ObjectOutputStream in = new ObjectOutputStream(
                        new FileOutputStream("lab5.bin"));
                try {
                    in.writeObject(orr);
                    in.close();
                } finally {in.close(); }
            }
            catch (FileNotFoundException e) {
                e.printStackTrace() ;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
    }
}
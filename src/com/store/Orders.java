package com.store;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;

public class Orders <T extends  Order> implements Serializable {
    private LinkedList<T> list;

    Orders() {
        list = new LinkedList<>();
    }

    public void add(T or) {
        list.add(or);
    }

    public void check_await() {
        Iterator iter = list.iterator();
        long time = System.currentTimeMillis();
        System.out.println("Время запуска обработки заказов " + time);
        while(iter.hasNext()) {
            T order = (T) iter.next();
            System.out.println("Заказ " + order.getId());
            if(order.check_time(time))
                System.out.println("Заказ обработан ");
            else
                System.out.println("Заказ не обработан ");
        }
    }

    public void check_process() {
        Iterator iter = list.iterator();

        while(iter.hasNext()) {
            T order = (T) iter.next();
            if(order.getStatus() == OrderStatus.PROCESSSED) {
                iter.remove();
            }
        }
    }

    public void delete(T or) { list.remove(or); }

    public void view_elemensts() {
        if(list.isEmpty()) {
            System.out.println("Заказов нет");
        }
        else {
            Iterator iter = list.iterator();
            System.out.println("Заказы");
            while(iter.hasNext()) {
                T order = (T) iter.next();
                System.out.println(order.getId() + " time_finish " + order.getFinish()
                + " " + order.getStatus());
            }
        }
    }

    public T getElByID(UUID id) {
        boolean findEl = false;
        Iterator iter = list.iterator();

        while(findEl == false && iter.hasNext()) {
            T order = (T) iter.next();
            if(order.getId().equals(id)) {
                return order;
            }
        }

        return null;
    }

    public T getByNumberList(int num) {
        if( num < list.size()) {
            T order = list.get(num);
            return order;
        }
        return null;
    }
}

package com.store;

public interface IcrudAction {
    void create();
    void read();
    void update();
    void delete();
}
